<?php 
require_once 'persistencia/Conexion.php';
require_once 'persistencia/estudianteDAO.php';

class estudiante{
    
    private $codigo;
    private $nombre;
    private $apellido;
    private $fecha_nacimiento;
    
    
    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @return string
     */
    public function getFecha_nacimiento()
    {
        return $this->fecha_nacimiento;
    }

    public function estudiante($codigo="", $nombre="", $apellido="",$fecha_nacimiento=""){
        
        $this->codigo=$codigo;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->fecha_nacimiento=$fecha_nacimiento;
        $this -> conexion = new Conexion();
        $this -> EstudianteDAO = new estudianteDAO($this->codigo, $this->nombre,$this->apellido,$this->fecha_nacimiento);
        
        

        
        
    }
    
    
    public function crear(){
        $this -> conexion -> abrir();
        echo $this->EstudianteDAO ->crear();
        $this -> conexion -> ejecutar($this -> EstudianteDAO -> crear());
        $this -> conexion -> cerrar();
    }
}


?>